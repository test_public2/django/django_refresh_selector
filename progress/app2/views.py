from django.shortcuts import render, redirect
from django.http import HttpResponse
from .application2 import MyApp2
from threading import Thread

my_app2 = MyApp2()


def app2(request):
    if request.POST:
        print('!!!!!!!!!!!!!!!!!!!!!')
        my_app2.reset_app()
        t = Thread(target=my_app2.start_app)
        t.start()

        return redirect('app2_started')
    return render(request, template_name='app2/app2.html')


def app2_started(request):
    return render(request, template_name='app2/app2_started.html')


def progress(request):
    p = my_app2.get_progress()
    print(p)
    if p == 100:
        return redirect('app1')
    context = {'data': [
        {'progress': p}
        ]}
    return HttpResponse(content=p)
