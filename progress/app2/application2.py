from time import sleep
from asgiref.sync import sync_to_async, async_to_sync


class MyApp2:

    def __init__(self):
        self.progress = 0

    def start_app(self):
        for i in range(0, 100):
            sleep(0.1)
            self.progress += 1

    def reset_app(self):
        self.progress = 0

    def get_progress(self):
        return self.progress
