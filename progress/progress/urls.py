from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('app1/', include('app1.urls')),
    path('app2/', include('app2.urls')),
    path('my-app/', include('my_app.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
