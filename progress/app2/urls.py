from django.urls import path
from . import views


urlpatterns = [
    path('', views.app2, name='app2'),
    path('started', views.app2_started, name='app2_started'),
    path('progress/', views.progress, name='app2_progress')
]
