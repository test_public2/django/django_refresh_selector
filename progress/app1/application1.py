from time import sleep


class MyApp1:

    def __init__(self) -> None:
        self.start: bool = False
        self.progress: int = 0
        self.status_list: list = []

    def start_app(self) -> None:
        self.start = True
        for i in range(0, 100):
            sleep(0.2)
            if i != 0 and i % 10 == 0:
                self.status_list.pop()
                self.status_list.append(f'Application {i // 10} - FINISHED')
            if i % 10 == 0:
                self.status_list.append(f'Application {i // 10 + 1} - STARTED')
            self.progress += 1
        self.start = False
        self.progress = 0
        self.status_list = []

    def get_progress(self) -> None:
        return self.progress

    def get_start(self) -> None:
        return self.start

    def get_status_list(self) -> None:
        return self.status_list
