from django.shortcuts import render, redirect
from django.http import HttpResponse
from .application1 import MyApp1
from threading import Thread

my_app1 = MyApp1()


def app1(request):
    if request.POST:
        if not my_app1.get_start():
            print('!!! START APPLICATION !!!')
            t = Thread(target=my_app1.start_app)
            t.start()
    context = {
        'started': my_app1.get_start(),
        'progress': my_app1.get_progress(),
        'status_list': my_app1.get_status_list()
    }
    return render(request, template_name='app1/app1.html', context=context)

