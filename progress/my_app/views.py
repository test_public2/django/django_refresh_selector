from django.shortcuts import render, redirect
from django.http import HttpResponse


class MyProg:
    def __init__(self):
        self.progress = 0

    def get_progress(self):
        if self.progress < 100:
            self.progress += 10
        else:
            self.progress = 100
        return self.progress


class ControlMyProg:
    def __init__(self):
        self.prog_dict = {}

    def gen_key(self):
        if len(self.prog_dict) == 0:
            return 0
        else:
            return list(self.prog_dict.keys())[-1] + 1

    def create_prog(self):
        p = MyProg()
        num = self.gen_key()
        self.prog_dict[num] = p
        return num

    def get_prog(self, num_prog):
        if num_prog < len(self.prog_dict):
            return self.prog_dict[num_prog]
        else:
            return None

    def delete_prog(self, num_prog):
        if num_prog < len(self.prog_dict):
            return self.prog_dict.pop(num_prog)

    def __len__(self):
        return len(self.prog_dict)


control = ControlMyProg()


def my_app(request):
    print(request.session.get('my_app', False))
    # if request.session.get('my_app', False):
    if request.POST:
        print("REQUEST POST")
        p = control.create_prog()
        print(f'CREATE NUMBER: {p} {len(control)}')
        request.session['my_app'] = p
        return render(request, template_name='my_app/my_app_start.html')
    return render(request, template_name='my_app/my_app.html')


def progress(request):
    p_num = request.session.get('my_app', False)
    prog = control.get_prog(p_num)
    print(f'GET PROG {p_num} {len(control)} ')
    if prog is not None:
        p = prog.get_progress()
    else:
        p = 'ERROR'
    if p == 100:
        control.delete_prog(p_num)
        del request.session['my_app']
        return HttpResponse(content=p)
    context = {'data': [
        {'progress': p}
        ]}
    return HttpResponse(content=p)
